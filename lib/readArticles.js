var Article = require('./Article'),
    Tag = require('./Tag'),
    fs = require('fs'),
    Promise = require('bluebird'),
    readFile = Promise.promisify(fs.readFile);

function hydrate(data) {
  Tag.load(data);
  return Article.load(data);
}

function format(articles) {
  var formatted = [];
  articles.forEach(function (article) {
    var display = article.name,
        tags = [];
    article.tags.forEach(function (tag) {
      tags = tags.concat(tag.flatten());
    });
    if (tags.length > 0) {
      display += ': ' + tags.join(', ');
    }
    formatted.push(display);
  });
  return formatted;
}

function readArticles(path, cb) {
  readFile(path)
    .then(JSON.parse)
    .then(hydrate)
    .then(format)
    .then(cb);
}

module.exports = readArticles;
