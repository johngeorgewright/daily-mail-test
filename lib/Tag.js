var Record = require('./Record'),
    util = require('util'),
    extend = util._extend,
    inherits = util.inherits,
    cache = {};

function Tag(attrs) {
  Record.call(this, attrs);
  this.init();
}

inherits(Tag, Record);

extend(Tag.prototype, {
  init: function () {
    // Some heavy work here
  },

  flatten: function () {
    var tags = [this.name];
    this.tags.forEach(function (tag) {
      tags = tags.concat(tag.flatten());
    });
    return tags;
  }
});

extend(Tag, {
  find: function (id) {
    return cache[id];
  },

  cache: function (attrs) {
    if (!this.find(attrs.id)) {
      var tag = new Tag(attrs);
      cache[tag.id] = tag;
    }
  },

  load: function (data) {
    data.forEach(function (item) {
      if (item.type === 'tag') {
        Tag.cache(item);
      }
    });
  }
});

module.exports = Tag;
