var util = require('util'),
    extend = util._extend,
    inherits = util.inherits,
    Record = require('./Record'),
    Tag = require('./Tag');

function Article(attrs) {
  Record.call(this, attrs);
}

inherits(Article, Record);

Article.load = function (data) {
  return data
    .filter(function (item) {
      return item.type === 'article';
    })
    .map(function (item) {
      return new Article(item);
    });
};

module.exports = Article;
