var extend = require('util')._extend;

function Record(attrs) {
  if (!attrs.id) {
    throw new Error('Records require an ID');
  }
  if (!attrs.name) {
    throw new Error('Records require a name');
  }
  extend(this, {
    id: attrs.id,
    name: attrs.name,
    tagRefs: attrs.tagRefs || []
  });
  this.hydrateTags();
}

Record.prototype.hydrateTags = function () {
  this.tags = this.tagRefs.map(Tag.find);
};

module.exports = Record;

var Tag = require('./Tag');
