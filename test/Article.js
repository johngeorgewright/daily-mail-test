var chai = require('chai'),
    expect = chai.expect,
    Article = require('../lib/Article');

describe('Article', function () {
  var article;

  beforeEach(function () {
    article = new Article({
      id: 1,
      name: 'some name',
      tagRefs: [1, 2]
    });
  });

  describe('.constructor()', function () {
    it('will create an empty tags array if one isn\'t provided', function () {
      var article = new Article({id: 1, name: 'name'});
      expect(article).to.have.property('tags');
      expect(article.tags).to.be.instanceof(Array);
      expect(article.tags).to.be.empty();
    });
  });
});
