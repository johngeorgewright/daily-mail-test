var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    Tag = require('../lib/Tag'),
    Record = require('../lib/Record');

chai.use(sinonChai);

describe('Tag', function () {
  describe('.constructor()', function () {
    var tag;

    beforeEach(function () {
      tag = new Tag({
        id: 1,
        name: 'some tag'
      });
    });

    it('should inherit from Record', function () {
      expect(tag).to.be.instanceof(Record);
    });
  });

  describe('.find()', function () {
    beforeEach(function () {
      Tag.cache({
        id: 1,
        name: 'some tag'
      });
    });

    it('will return a tag object', function () {
      expect(Tag.find(1)).to.be.instanceof(Tag);
    });
  });

  describe('.load()', function () {
    beforeEach(function () {
      sinon.spy(Tag, 'cache');
    });

    beforeEach(function () {
      Tag.load([
        {
          id: 1,
          name: 'Tag 1',
          type: 'tag'
        },
        {
          id: 2,
          name: 'Tag 2',
          type: 'tag'
        }
      ]);
    });

    it('will cache each item', function () {
      expect(Tag.cache).to.have.been.calledTwice;
    });
  });
});
