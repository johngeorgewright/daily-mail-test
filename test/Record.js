var chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    Record = require('../lib/Record'),
    Tag = require('../lib/Tag');

chai.use(sinonChai);

describe('Record', function () {
  describe('.constructor()', function () {
    var attrs;

    function construct() {
      return new Record(attrs);
    }

    beforeEach(function () {
      attrs = {
        id: 1,
        name: 'some name'
      };
    });

    it('will throw an error if no id is given', function () {
      delete attrs.id;
      expect(construct).to.throw(Error, 'Records require an ID');
    });

    it('will throw an error if no name is given', function () {
      delete attrs.name;
      expect(construct).to.throw(Error, 'Records require a name');
    });

    it('will create an record object using the given attributes', function () {
      var record = construct();
      expect(record).to.be.instanceof(Record);
      expect(record).to.have.property('id', attrs.id);
      expect(record).to.have.property('name', attrs.name);
    });

    it('will call #hydrateTags()', function () {
      sinon.spy(Record.prototype, 'hydrateTags');
      var record = construct();
      expect(record.hydrateTags).to.have.been.called;
    });
  });

  describe('#hydrateTags()', function () {
    var record;

    beforeEach(function () {
      var tag = new Tag({id: 1, name: 'tag'});
      sinon.stub(Tag, 'find').returns(tag);
    });

    beforeEach(function () {
      record = new Record({
        id: 1,
        name: 'some name',
        tagRefs: [1, 2]
      });
    });

    afterEach(function () {
      Tag.find.restore();
    });

    it('will hydrate the tags in to Tag objects', function () {
     record.tags.forEach(function (tag) {
        expect(tag).to.be.instanceof(Tag);
      });
    });
  });
});
