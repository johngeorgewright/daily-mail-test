Articles test
=============

Unfortunately, I was given an already working test. Not knowing what was
supposed to be there, I just wiped everything and started over.

Installing
----------

1. Clone the repo
2. Install dependencies `npm i`

Running
-------

Specs can be run with `npm test`. The desired output can be run with `npm run
articles`.
